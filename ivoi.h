#ifndef IVOI_H
#define IVOI_H

#include <stdint.h>

enum TelegramType {
    Specification = 1,
    Data = 2,
    Debug = 3,
    Unknown = -1
};

struct DataRequest {
    uint8_t length;
    uint8_t data[256];
    union {
        uint16_t crc;
        struct {
            uint8_t crc_lo;
            uint8_t crc_hi;
        };
    };
    enum TelegramType type;
};

struct DataRequestMessage {
    uint8_t req_ct;
    uint8_t nwaves;
    uint8_t waveidx[2];
};

struct __attribute__((__packed__)) StatusRecord {
    uint8_t alarm_state;
    uint8_t general_inop;
    uint8_t inop;
    uint8_t alarm;
    uint32_t device_id;
};

struct DataRequest parseTelegram(uint8_t rxbuf[], uint8_t rxlen);
struct DataRequestMessage parseDataRequest(uint8_t data[], uint8_t len);
void sendDataResponse(struct DataRequestMessage request, uint64_t wave, uint16_t numeric);
uint16_t calcTelegramCrc(struct DataRequest telegram);
uint16_t calcCrc(uint8_t data[], uint8_t len, uint16_t init);
void sendSpecification(void);

#endif