#ifndef GLOBAL_H
#define GLOBAL_H

#include "auxuart.h"

#define DEVICEID 0x908c74bcUL

#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))
#define CPU_125kHz      0x07
#define CPU_16MHz       0x00

#define AUX_PRINT(...) aux_printf(__VA_ARGS__)
#define AUX_PRINTHEX(message, len) aux_printhex(message, len)

#define BSWAP_16(num) ((num>>8) | (num<<8));

#ifdef DEBUG
    #define DEBUG_PRINT(...) aux_printf(__VA_ARGS__)
    #define DEBUG_PRINTHEX(message, len) aux_printhex(message, len)
#else
    #define DEBUG_PRINT(...) do {} while (0)
    #define DEBUG_PRINTHEX(message, len) do {} while (0)
#endif

#endif
