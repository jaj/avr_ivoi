#ifndef IVOIUART_H
#define IVOIUART_H

#include <stdint.h>
#include <stdbool.h>

#define RX_ISR_ENABLE  (UCSR1B |= _BV(RXCIE1))
#define RX_ISR_DISABLE (UCSR1B &= ~(_BV(RXCIE1)))

struct RxBuffer {
    uint8_t buffer[256];
    uint8_t bufferlen;
    uint8_t datalen;
};

void ivoi_init(void);
void ivoi_rxReset(void);
void ivoi_transmit(uint8_t data);
void ivoi_send(uint8_t buffer[], uint8_t len);
bool ivoi_isTelegramAvailable(void);
struct RxBuffer ivoi_getRxBuffer(void);

#endif