#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <avr/io.h>

#include "global.h"
#include "auxuart.h"

void auxuart_init(void)
{
    /* Baud rate 19200 */
    // F_CPU = 16 MHz
	// UBRR1 = (F_CPU / 4 / baud - 1) / 4;
    UBRR0 = 51;
    // 8 data bits, 1 stop bit, no parity
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
    // Enable TX
    UCSR0B = _BV(TXEN0);
}

void auxuart_transmit(uint8_t data)
{
    // UDREn: uart Data Register Empty
    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = data;
}

int auxuart_transmit_wrapper(char c, FILE *fd)
{
    if (c == '\n')
        auxuart_transmit('\r');
    auxuart_transmit((uint8_t)c);

    return 0;
}

static FILE auxuartfd = FDEV_SETUP_STREAM(auxuart_transmit_wrapper, 0, _FDEV_SETUP_WRITE);

void aux_printf(const char* format, ...)
{
    va_list argptr;
    va_start(argptr, format);
    vfprintf(&auxuartfd, format, argptr);
    va_end(argptr);
}

void aux_printhex(uint8_t message[], uint8_t len)
{
    for(uint8_t i = 0; i < len; i++)
        fprintf(&auxuartfd, "%02X", message[i]);
}
