#include <stdint.h>
#include <string.h>

#include <avr/pgmspace.h>
#include <util/crc16.h>

#include "global.h"
#include "spec.h"
#include "ivoi.h"
#include "ivoiuart.h"

static struct StatusRecord statusrecord = { .alarm_state = true, .device_id = DEVICEID };

struct DataRequest parseTelegram(uint8_t buffer[], uint8_t datalen)
{
    struct DataRequest currequest;
    uint8_t frametype = buffer[0];
    currequest.length = datalen;
    memcpy(currequest.data, (const void *)buffer+2, currequest.length);
    currequest.crc_hi = buffer[datalen + 2];
    currequest.crc_lo = buffer[datalen + 3];

    switch(frametype)
    {
        case 1:
            currequest.type = Specification;
            break;
        case 2:
            currequest.type = Data;
            break;
        case 3:
            currequest.type = Debug;
            break;
        default:
            currequest.type = Unknown;
    }

    ivoi_rxReset();

    return currequest;
}

struct DataRequestMessage parseDataRequest(uint8_t data[], uint8_t len)
{
    struct DataRequestMessage datamsg;
    datamsg.req_ct = data[0];
    switch(len)
    {
        case 3:
            datamsg.nwaves = 2;
            datamsg.waveidx[0] = data[1];
            datamsg.waveidx[1] = data[2];
            break;
        case 2:
            datamsg.nwaves = 1;
            datamsg.waveidx[0] = data[1];
            break;
        default:
            datamsg.nwaves = 0;
            break;
    }
    return datamsg;
}

void sendDataResponse(struct DataRequestMessage request, uint64_t wave, uint16_t numeric)
{
    uint8_t buffer[256];
    uint8_t buflen = 0;
    uint16_t crc;

    memcpy(buffer, "\x82\x00", 2);
    buflen += 2;

    for (uint8_t i = 0; i < request.nwaves; ++i)
    {
        memcpy(buffer + buflen, &wave, sizeof(wave));
        buflen += sizeof(wave);
    }

    switch(request.req_ct)
    {
        case 0:
            // request for status record
            memcpy(buffer + buflen, (uint8_t*)&statusrecord, sizeof(statusrecord));
            buflen += sizeof(statusrecord);
            break;

        case 1:
            // request for numeric record
            numeric = BSWAP_16(numeric);
            memcpy(buffer + buflen, &numeric, sizeof(numeric));
            buflen += sizeof(numeric);
            break;

        default:
            ;
    }

    // Set data length
    buffer[1] = buflen - 2;

    crc = calcCrc(buffer, buflen, 0);
    crc = BSWAP_16(crc);

    memcpy(buffer + buflen, &crc, sizeof(crc));
    buflen += sizeof(crc);

    ivoi_send(buffer, buflen);
}

uint16_t calcTelegramCrc(struct DataRequest telegram)
{
    uint16_t crc;

    crc = _crc_xmodem_update(0, telegram.type);
    crc = _crc_xmodem_update(crc, telegram.length);
    crc = calcCrc(telegram.data, telegram.length, crc);
    return crc;
}

uint16_t calcCrc(uint8_t data[], uint8_t len, uint16_t init)
{
    uint16_t crc = init;
    for(uint8_t i = 0; i < len; i++)
        crc = _crc_xmodem_update(crc, data[i]);
    return crc;
}

void sendSpecification(void)
{
    uint8_t b;
    uint16_t specsize;

    specsize = pgm_read_word(&___spec_spectabl_bin_len);
    for(uintptr_t i = 0; i < specsize; i++)
    {
        b = pgm_read_byte(___spec_spectabl_bin + i);
        ivoi_transmit(b);
    }
}
