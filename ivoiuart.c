#include <stdint.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "global.h"
#include "ivoiuart.h"

// UART receive buffer
volatile uint8_t rxbuf[256];
volatile uint8_t rxbufi = 0;
volatile uint8_t rxlen = 0;
volatile bool telegramAvailable = false;

ISR(USART1_RX_vect)
{
    uint8_t c = UDR1;
    rxbuf[rxbufi] = c;

    if(rxbufi == 0 && c != 1 && c != 2 && c != 3)
        return; // Unknown frame type, ignore

    if(rxbufi == 1)
        rxlen = c;

    rxbufi++;

    if(rxbufi >= (rxlen + 4))
    {
        RX_ISR_DISABLE;
        telegramAvailable = true;
    }
}

void ivoi_init(void)
{
    /* Baud rate 19200 */
    // F_CPU = 16 MHz
	// UBRR1 = (F_CPU / 4 / baud - 1) / 4;
    UBRR1 = 51;
    // 8 data bits, 1 stop bit, no parity
    UCSR1C = _BV(UCSZ11) | _BV(UCSZ10);
    // Enable RX and TX
    UCSR1B = _BV(RXEN1)| _BV(TXEN1);
    RX_ISR_ENABLE;
}

void ivoi_rxReset(void)
{
    telegramAvailable = false;
    rxbufi = 0;
    rxlen = UINT8_MAX;
    RX_ISR_ENABLE;
}

void ivoi_send(uint8_t buffer[], uint8_t len)
{
    uint8_t i;
    for(i = 0; i < len; i++)
        ivoi_transmit(buffer[i]);
}

void ivoi_transmit(uint8_t data)
{
    // UDREn: uart Data Register Empty
    loop_until_bit_is_set(UCSR1A, UDRE1);
    UDR1 = data;
}

bool ivoi_isTelegramAvailable(void)
{
    return telegramAvailable;
}

struct RxBuffer ivoi_getRxBuffer(void)
{
    struct RxBuffer buf;
    buf.datalen = rxlen;
    buf.bufferlen = rxlen + 4;
    memcpy(buf.buffer, (const void*)rxbuf, buf.bufferlen);
    return buf;
}
