#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/crc16.h>

#include "global.h"
#include "ivoi.h"
#include "ivoiuart.h"

// 64 bit ring buffer of 4x16 bit values
volatile uint64_t wavbuf = 0;

// 2 second moving average of wave
uint8_t numcount = 0;
volatile uint32_t numbuf = 0;
volatile uint16_t numres = 0;

// Fill wavbuf from ADC at 125 Hz
ISR(TIMER1_COMPA_vect)
{
    do {} while (ADCSRA & _BV(ADSC));
    uint64_t val = ADCW;
    ADCSRA |= _BV(ADSC);

    wavbuf >>= 16;
    wavbuf |= (val << 48);

    numbuf += val;
    numcount++;

    if(numcount >= 250)
    {
        numres = numbuf / numcount;
        numbuf = numcount = 0;
    }
}

// 1 Hz test signal
ISR(TIMER3_COMPA_vect)
{
    PORTB ^= _BV(PORTB0);
}

int main(int argc, char *argv[])
{
    // Configure ADC
    // Single conversion. 2.56V reference. ADC0.
    DIDR0 = _BV(ADC0D);
    PRR0 &= ~_BV(PRADC);
    ADCSRA = _BV(ADEN);
    ADCSRA |= _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
    ADMUX = _BV(REFS0) | _BV(REFS1);

    // Start first conversion. Next come from timer ISR.
    ADCSRA |= _BV(ADSC);

    // Timer 1 configuration for 125 Hz
    OCR1A = 15999; // clock 16 MHz / 8, timer 125 Hz
    TIMSK1 = _BV(OCIE1A); // Enable CTC interrupt
    TCCR1B = _BV(CS11) | _BV(WGM12); // CTC mode, prescaler = 8

    // Timer 3 configuration for 1 Hz
    OCR3A = 15624; // clock 16 MHz / 1024, timer 1 Hz
    TIMSK3 = _BV(OCIE3A); // Enable CTC interrupt
    TCCR3B = _BV(CS30) | _BV(CS32) | _BV(WGM32); // CTC mode, prescaler = 1024

    // Configure PB0 for test signal at 1 Hz
    DDRB = _BV(DDB0);

    ivoi_init();
    auxuart_init();

    struct DataRequest telegram;
    struct DataRequestMessage datareqmsg;
    struct RxBuffer rxbuffer;
    uint16_t crc;

    sei();

    for (;;)
    {
        if(!ivoi_isTelegramAvailable())
            continue;

        rxbuffer = ivoi_getRxBuffer();
        telegram = parseTelegram(rxbuffer.buffer, rxbuffer.datalen);
        crc = calcTelegramCrc(telegram);

        if(crc != telegram.crc)
        {
            AUX_PRINT("E");
            DEBUG_PRINT("\nCRC error: %x != %x\n", crc, telegram.crc);
            continue;
        }

        switch (telegram.type)
        {
            case Specification:
                AUX_PRINT("S");
                sendSpecification();
                break;

            case Data:
                AUX_PRINT(".");
                datareqmsg = parseDataRequest(telegram.data, telegram.length);
                sendDataResponse(datareqmsg, wavbuf, numres);
                break;

            case Debug:
                AUX_PRINT("D");
                DEBUG_PRINT("\n<");
                DEBUG_PRINTHEX(telegram.data, telegram.length);
                DEBUG_PRINT(">\n");
                break;

            default:
                AUX_PRINT("U");
        }
    }

    return 0;
}
