#ifndef AUXUART_H
#define AUXUART_H

#include <stdint.h>
#include <stdio.h>

void auxuart_init(void);
void auxuart_transmit(uint8_t data);
int auxuart_transmit_wrapper(char c, FILE *fd);

void aux_printf(const char* format, ...);
void aux_printhex(uint8_t message[], uint8_t len);

#endif